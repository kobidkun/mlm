<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Epin;
use Auth;
class epinController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function generateepin (Request $request ) {
        $newepin =  uniqid(rand(), true);

        $epin = new Epin();
        $epin -> epin = $newepin;
        $epin -> user_id = Auth::user()->id;
        $epin->save();
        return redirect('viewepin');

    }

    public function viewepin() {

        $epins = Epin::orderBy('id', 'desc')->paginate(300);

        return view('user.show')->withEpins($epins);
    }
}
