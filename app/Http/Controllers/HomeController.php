<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
//use App\Http\Requests;

use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::orderBy('id', 'desc')->paginate(300);
        return view('dashboard')->withAdmins($admins);

      //  $posts = Post::orderBy('id', 'desc')->paginate(10);
       // return view('dashboard')->withPosts($posts);
    }

    public function show($id)
    {
        $admins = Admin::orderBy('id', 'desc')->paginate(300);
        return view('dashboard')->withAdmins($admins);
    }

    public function edit($id)
    {
        // find the post in the database and save as a var
        $admin = Admin::find($id);

        // return the view and pass in the var we previously created
        return view('user.edit')->withAdmin($admin);
    }

    public function update(Request $request, $id)
    {
 
         
        $admin = Admin::find($id);


        $admin->earning = $request->earning;


        $admin->save();




        return redirect('home');
 
    }
}
