<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Admin;
use Auth;

class ProfileUpdate extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {




        return view('admin.pages.profile');

}

    public function edit($id)
    {
        // find the post in the database and save as a var
        $admin = Admin::find($id);

        // return the view and pass in the var we previously created
        return view('admin.pages.profile')->withAdmin($admin);
    }

    public function update(Request $request, $id)
    {




        $admin = Admin::find($id);

        $photoName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads'), $photoName);



        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->address = $request->address;
        $admin->bank_acc = $request->bank_acc;
        $admin->bank_name = $request->bank_name;
        $admin->ifsc = $request->ifsc;
        $admin->img = $photoName;

        $admin->save();





        return redirect('admin');

    }



    public function updateprofilepicture(Request $request, $id)
    {




        $admin = Admin::find($id);

        $photoName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads'), $photoName);




        $admin->img = $photoName;

        $admin->save();





        return redirect('admin');

    }





/*
public function profileupdate() {

}*/



}
