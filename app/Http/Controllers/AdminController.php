<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
    {

        {
           

            return view('admin.pages.dashboard');
        }

        // return view('admin');
    }
    
       public function edit($id)
    {
        // find the post in the database and save as a var
        $admin = Admin::find($id);
        // return the view and pass in the var we previously created
        return view('admin')->withAdmin($admin);
    }
    

    public function profile() {




        return view('admin.pages.profile');

}

    
 
}
