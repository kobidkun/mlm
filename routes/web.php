<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('indipay/response','Auth\register@response');
Route::post('indipay/response','Auth\register@response');



//Route::get('success', 'Auth\register@response')->name('admin.success');


/*Route::get('admin/register', function () {
    return view('admin.register');
});

Route::delete('admin/register', function(){
    re
});

Route::post('admin/register', 'Auth\register@store')->name('register.submit');*/

Route::resource('admin/register','Auth\register');

Auth::routes();
Route::get('/epin', 'epinController@generateepin')->name('admin.epin');
Route::get('/viewepin', 'epinController@viewepin')->name('admin.viewepin');



Route::resource('home', 'HomeController');

Route::prefix('admin')->group(function (){

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/updateprofilepicture', 'ProfileUpdate@updateprofilepicture')->name('image.dashboard');

   // Route::put('/', 'AdminController@update')->name('admin.update.submit');
    //Route::get('/profile', 'AdminController@profile');
    Route::resource('/profile', 'ProfileUpdate');
    
 //   Route::get('/', 'AdminController@index');ProfileUpdate

});
