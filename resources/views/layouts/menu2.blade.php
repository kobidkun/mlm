<body id="top" class="has-header-search">
<!-- Top bar -->
<div class="top-bar light-blue visible-md visible-lg">
    <div class="container">
        <div class="row">
            <!-- Social Icon -->
            <div class="col-md-6">
                <!-- Social Icon -->
                <ul class="list-inline social-top tt-animate btt">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>

            <div class="col-md-6 text-right">
                <ul class="topbar-cta no-margin">
                    <li class="mr-20">
                        <a><i class="material-icons mr-10">&#xE0B9;</i>info@trucklogi.com</a>
                    </li>
                    <li>
                        <a><i class="material-icons mr-10">&#xE0CD;</i> +91-9977284396</a>
                    </li>
                </ul>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>


<!--header start-->
<header id="header" class="tt-nav transparent-header">

    <div class="header-sticky light-header">

        <div class="container">





            <div id="materialize-menu" class="menuzord">

                <!--logo start-->
                <a href="/" class="logo-brand">
                    <img class="logo-dark" src="img/logolight.png" alt=""/>
                    <img class="logo-light" src="img/logo.png" width="250" alt=""/>
                </a>
                <!--logo end-->

                <!--mega menu start-->
                <ul class="menuzord-menu pull-right light">
                    <li><a href="/">Home</a>
                    </li>

                    <li><a href="javascript:void(0);">Services</a>
                        <ul class="dropdown">
                            <li><a href="gps-tracking.php">GPS Enquiry</a>
                            </li>
                            <li><a href="#">Vehicle Load Enquiry</a>
                            </li>
                            </li>
                            <li><a href="vehicle-insurance.php">Vehicle Insurance</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="about-us.php">About us</a>
                    </li>
                    <li><a href="contact-us.php">Contact us</a>
                    </li>
                    <li>
                        <a href="login.php">Login / Register</a>
                    </li>
                </ul>
                <!--mega menu end-->

            </div>
        </div>
    </div>

</header>
<!--header end-->
