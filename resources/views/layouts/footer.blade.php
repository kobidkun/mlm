<!--footer 4 start -->
<footer class="footer footer-four">
    <div class="primary-footer brand-bg text-center">
        <div class="container">

            <a href="#top" class="page-scroll btn-floating btn-large pink back-top waves-effect waves-light tt-animate btt" data-section="#top">
                <i class="material-icons">&#xE316;</i>
            </a>

            <ul class="social-link tt-animate ltr mt-20">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul>

            <hr class="mt-15">

            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <img src="img/logo.png" alt="">
                    </div>

                    <span class="copy-text">Copyright &copy; 2016 <a href="#">TruckLogi</a> &nbsp; | &nbsp;  All Rights Reserved</span>

                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.primary-footer -->

    <div class="secondary-footer brand-bg darken-2 text-center">
        <div class="container">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Contact us</a></li>
            </ul>
        </div><!-- /.container -->
    </div>
    <!-- /.secondary-footer -->
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large orange">
            <i class="material-icons">feedback</i>
        </a>
        <ul>
            <li><a class="btn-floating red"><i class="material-icons">call</i>jhjhjjj</a></li>
            <li><a class="btn-floating yellow darken-1"><i class="material-icons">add_location</i></a></li>
        </ul>
    </div>
</footer>
<!--footer 4 end-->






<!-- jQuery -->
<script src="js/build.js"></script>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });
</script>