@extends('admin.index')

@section('content')
    <div id="main-wrapper">
        <div class="row">

            <div class="col-md-12">

                <form action="{{url('admin/profile/' . $admin->id)}}"  enctype="multipart/form-data" method="post" >
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">



        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" value="{{ $admin->name }}" required class="form-control">
        </div>

                    <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" required value="{{ Auth::user()->email }}"  class="form-control">
        </div>

        <div class="form-group">
            <label for="name">Mobile No</label>
            <input type="text" name="mobile" required value="{{ $admin->mobile }}"   class="form-control">
        </div>

        <div class="form-group">
            <label for="name">Address</label>
            <input type="text" name="address" required  value="{{ $admin->address }}"   class="form-control">
        </div>

       <div class="form-group">
            <label for="name">Bank Acc No</label>
            <input type="text" name="bank_acc" required value="{{ $admin->bank_acc }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="name">Bank Name</label>
            <input type="text" name="bank_name" required  value="{{ $admin->bank_name }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="name">IFSC Code</label>
            <input type="text" name="ifsc" required value="{{ $admin->ifsc }}" class="form-control">
        </div>


        <div >

           <img class="center" src="../../../../uploads/{{ $admin->img }}" width="250px" >
        </div>











        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i>Update
        </button>


    </form>

                <br>
                <br>
                <br>

                <a href="admin/updateprofilepicture" type="button" class="btn btn-primary">Update Image</a>
            </div>
        </div>
    </div>

    @endsection