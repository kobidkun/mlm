@extends('layouts.structure')

@section('content')
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Online Vehicle Insurance</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active">Insurance</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="padding-top-110">
    <div class="container">

        <div class="row">
            <div class="col-md-7">
                <h2 class="text-bold mb-30">Commercial Vehicles Insurance</h2>

                <p style="text-align: justify;">Vehicles are always treated as a very important asset of our lives. But again driving a vehicle is always dangerous and one has to be careful and cautious always. But accidents might happen at any point of time due to someone else not following the traffic rules or due to some untoward incidents. And that is why one should avail commercial vehicles insurance plans to protect oneself and to cover from all unexpected expenses. Not only this, but natural calamities can also occur at any point of time and hence protection/ security against such unpleasant incident is also of utmost important. Hence to avail commercial vehicles insurance plans are important because:</p>


            </div>

            <div class="col-md-5">
                <img src="img/vi.jpg" class="img-responsive " alt="Image">
            </div>
        </div>

    </div><!-- /.container -->


    </div>
</section>

<section>

    <div class="container">

        <div class="text-center mb-80">
            <h2 class="section-title text-uppercase">Fill the form to buy insurance</h2>
        </div>

        <div class="row">
            <div class="col-md-8">
                <form name="contact-form" id="contactForm" action="" method="POST">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <input type="text" name="name" class="validate" id="name">
                                <label for="name">Name</label>
                            </div>

                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                            <div class="input-field">
                                <label class="sr-only" for="email">Email</label>
                                <input id="email" type="email" name="email" class="validate" >
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <input id="phone" type="tel" name="phone" class="validate" >
                                <label for="phone">Phone Number</label>
                            </div>
                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                            <div class="input-field">
                                <input id="website" type="text" name="website" class="validate" >
                                <label for="website">Truck NO</label>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="input-field">
                        <textarea name="message" id="message" class="materialize-textarea" ></textarea>
                        <label for="message">Message</label>
                    </div>

                    <button type="submit" name="submit" class="waves-effect waves-light btn submit-button pink mt-30">Send Message</button>
                </form>
            </div>

            <div class="col-md-4 contact-info">
                <h2 class="text-bold mb-30">Or Call Us</h2>

                <address>
                    <i class="material-icons brand-color">&#xE61C;</i>
                    <div class="phone">
                        <p>
                            Phone: +91-9977284396</p>
                    </div>

                    <i class="material-icons brand-color">&#xE0E1;</i>
                    <div class="mail">
                        <p>
                            <a href="#">support@trucklogi.com</a></p>
                    </div>
                </address>

            </div><!-- /.col-md-4 -->

        </div>
    </div>
</section>

@endsection