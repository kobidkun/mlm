@extends('layouts.structure')

@section('content')
<section class="page-title ptb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Online Vehicle Tracking</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active">Tracking</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="padding-top-110">
    <div class="container">

        <div class="row">
            <div class="col-md-7">
                <h2 class="text-bold mb-30">GPS Fleet Truck Tracking Systems</h2>

                <p style="text-align: justify;">he right GPS tracking system makes tracking company vehicles simple and effective. GPS truck tracking is becoming more common as businesses and government agencies recognize the benefits of these systems. With improved coordination of assets, faster delivery times and better fleet management, truck tracking systems can dramatically improve efficiencies.</p>
                <p>Track Your Truck's truck tracking systems have the right technology and equipment to make it easier than ever for fleet management professionals to effectively cut costs and improve service. Track Your Truck’s system allows fleet managers to observe vehicles remotely, receiving information about vehicle location, direction of travel and status of unit. Historically, Track Your Truck’s reporting system allows you to access the amount of detail needed to observe and correct unwanted trends and determine and lower fuel usage. Track Your Truck provides real time data, with as low as 10 second intervals, allowing fleet managers to direct their fleets efficiently.</p>


            </div>

            <div class="col-md-5">
                <img src="img/gps.jpg" class="img-responsive " alt="Image">
            </div>
        </div>

    </div><!-- /.container -->


    </div>
</section>

@endsection