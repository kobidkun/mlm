@extends('layouts.structure')

@section('content')

<title>About us Truck Logi</title>



<section class="section-padding banner-11 parallax-bg bg-fixed overlay light-8" data-stellar-background-ratio="0.1">
    <div class="container">

        <div class="text-center mb-80">
            <h1 class="section-title text-uppercase">Why TruckLogi</h1>
            <h2 class="section-sub">"Trucklogi helps truckers and transporters to do business transactions
                in a more transparent, efficient and profitable manner ensuring a much
                better experience for all parties concerned."

                We understand that every goods has different type of requirements. We
                have designed a product-focused transport solutions for following
                goods"
            </h2>

            <br>
            <div class="row">

                <h4 class=""> We understand that every goods has different type of requirements. We
                    have designed a product-focused transport solutions for following
                    goods</h4>

                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/aa/1.jpg" alt="image">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator">Industrial goods</span>
                            <p><a href="#"></a></p>
                        </div>

                    </div><!-- /.card -->

                </div>


                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/aa/2.jpg" alt="image">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator">Commercial goods</span>
                            <p><a href="#"></a></p>
                        </div>

                    </div><!-- /.card -->

                </div>


                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/aa/3.gif" alt="image">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator">Warehousing</span>
                            <p><a href="#"></a></p>
                        </div>

                    </div><!-- /.card -->

                </div>


                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/aa/4.jpg" alt="image">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator">E-Commerce</span>
                            <p><a href="#"></a></p>
                        </div>

                    </div><!-- /.card -->

                </div>





            </div>

        </div>


    </div>
</section>

@endsection