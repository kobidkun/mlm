<!-- form -->
<div class="container homepageform">
    <div class="row">
        <section>
            <div class="wizard">
                <div class="wizard-inner">

                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab"
                               title="Step 1">
                            <span class="round-tab">
                                <i class="material-icons">&#xE55F;</i>
                            </span>
                            </a>
                        </li>


                        <li role="presentation" class="disabled">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab"
                               title="Complete">
                            <span class="round-tab">
                                <i class="material-icons">&#xE55A;</i>
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                    <form role="form" method="post" role="form" method="POST" action="{{ route('post.submit') }}">
                        {{csrf_field()}}
                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">&#xE55E;</i>
                                        <input id="txtSource" placeholder="Enter your address"
                                               name="from_loc" required type="text" class="validate">
                                        <label for="icon_telephone">From</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">&#xE55E;</i>
                                        <input id="txtDestination" required placeholder="Enter your address"
                                               name="to_loc" type="text" class="validate">
                                        <label for="icon_telephone">To</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">&#xE558;</i>
                                        <select class="icons" id="txt1" name="truck_type" required>
                                            <option value="" disabled selected>Select Vehicle Type</option>
                                            <option value="51.2" name="Road Tankers 10 wheel" data-icon="img/sldtrk/1.jpg"
                                                    class="left circle">Road Tankers(10 wheel)
                                            </option>
                                            <option value="45" name="Road Tankers(6 wheel)" data-icon="img/sldtrk/2.jpg"
                                                    class="left circle">Road Tankers(6 wheel)
                                            </option>
                                            <option value="51.2" name="Truck 6 wheel" data-icon="img/sldtrk/3.jpg"
                                                    class="left circle">Truck (6 wheel)
                                            </option>
                                            <option value="51.2" name="Truck 10 wheel 22 Feet " data-icon="img/sldtrk/4.jpg"
                                                    class="left circle">Truck 10 wheel (22 Feet )
                                            </option>
                                            <option value="60.09" name="Truck 12 wheel 22 Feet " data-icon="img/sldtrk/5.jpg"
                                                    class="left circle">Truck 12 wheel (22 Feet )
                                            </option>
                                            <option value="64.8" name="Trailer 14 wheel" data-icon="img/sldtrk/6.jpg"
                                                    class="left circle">Trailer (14 wheel)
                                            </option>
                                            <option value="71.55" name="Trailer 18 wheel" data-icon="img/sldtrk/7.jpg"
                                                    class="left circle">Trailer 18 wheel
                                            </option>
                                            <option value="79.2" name="Trailer 22 wheel" data-icon="img/sldtrk/7.jpg"
                                                    class="left circle">Trailer 22 wheel
                                            </option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">&#xE8A7;</i>
                                            <select class="icons" id="txt1" name="material" required>
                                                <option value="" disabled selected>Select Material Type</option>
                                                <option value="41">Auto Parts</option>
                                                <option value="45">Bardana jute or plastic</option>
                                                <option value="18">Building Materials</option>
                                                <option value="3">Cement</option>
                                                <option value="4">Chemicals</option>
                                                <option value="16">Coal And Ash</option>
                                                <option value="6">Container</option>
                                                <option value="43">Cotton seed</option>
                                                <option value="5">Electronics Consumer Durables</option>
                                                <option value="7">Fertilizers</option>
                                                <option value="10">Fruits And Vegetables</option>
                                                <option value="11">Furniture And Wood Products</option>
                                                <option value="12">House Hold Goods</option>
                                                <option value="13">Industrial Equipments</option>
                                                <option value="17">Iron sheets or bars or scraps</option>
                                                <option value="24">Liquids in drums</option>
                                                <option value="15">Liquids/Oil</option>
                                                <option value="26">Machinery new or old</option>
                                                <option value="25">Medicals</option>
                                                <option value="46">Metals</option>
                                                <option value="27">Mill Jute Oil</option>
                                                <option value="21">Others</option>
                                                <option value="9">Packed Food</option>
                                                <option value="23">Plastic Pipes or other products</option>
                                                <option value="29">powder bags</option>
                                                <option value="22">Printed books or Paper rolls</option>
                                                <option value="8">Refrigerated Goods</option>
                                                <option value="1">Rice or wheet or Agriculture Products</option>
                                                <option value="14">Scrap</option>
                                                <option value="31">Spices</option>
                                                <option value="19">Textiles</option>
                                                <option value="20">Tyres And Rubber Products</option>
                                                <option value="2">Vehicles or car</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">&#xE558;</i>
                                        <input type="date" name="date" class="datepicker">

                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">&#xE192;</i>

                                            <input type="time" name="time" class="timepicker">

                                        </div>
                                    </div>
                                </div>

                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" onclick="GetRoute()"
                                                class="btn btn-primary next-step z-depth-5 waves-effect waves-light btn">
                                            Save and continue
                                        </button>
                                    </li>
                                </ul>
                            </div>


                            <div class="tab-pane" role="tabpanel" id="complete">
                                <h3 class="textcenterhome">Enter your Contact Details</h3>
                                <div class="row">
                                    <div class="input-field col s6" id="locationField">
                                        <i class="material-icons prefix">&#xE0BA;</i>
                                        <input required name="fname"
                                               type="text" class="validate">
                                        <label for="icon_telephone">First Name</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">&#xE0BA;</i>
                                        <input name="lname"
                                               type="text"
                                               class="validate">
                                        <label for="icon_telephone">Last Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6" id="locationField">
                                        <i class="material-icons prefix">&#xE0D0;</i>
                                        <input
                                                required
                                                value=""
                                                name="username" type="email" class="validate">
                                        <label for="icon_telephone">Email id</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">&#xE0D1;</i>
                                        <input required
                                               value=""
                                               name="phone1"
                                               type="number" class="validate">
                                        <label for="icon_telephone">Mobile No</label>
                                    </div>
                                </div>






                                <div class="col s12" id="map_dc">

                                    <div id="dvMap" style="width:700px; height: 150px"></div>
                                    <div class="counter-section color-box-counter beforemap">
                                        <div class="row text-center">
                                            <div class="col-sm-4">
                                                <div class="color-box z-depth-5 blue-grad border-slider-form">
                                                    <i class="Small material-icons center small">access_time</i>
                                                    <div class="time-dist">
                                                        <h3  class="count-desc" style="color: white;  padding-top: 2px;">Estimated Time</h3>
                                                        <h3 id="demo" class="count-desc" style="color: white; font-weight: bold"></h3>
                                                    </div>

                                                </div>
                                            </div> <!-- /.col-sm-3 -->

                                            <div class="col-sm-4">
                                                <div class="color-box  z-depth-5 pink-grad border-slider-form">
                                                    <i class="Small material-icons center ">local_shipping</i>
                                                    <div class="time-dist">
                                                        <h3  class="count-desc" style="color: white; padding-top: 2px;">Estimated Distance</h3>
                                                        <h3 id="dvDistance" class="count-desc" style="color: white; font-weight: bold"></h3>
                                                    </div>

                                                </div>
                                            </div><!-- /.col-sm-3 -->

                                            <div class="col-sm-4">
                                                <div class="color-box z-depth-5 purple-grad border-slider-form">
                                                    <i class="Small material-icons center small">access_time</i>
                                                    <div class="time-dist">
                                                        <h3  class="count-desc" style="color: white;  padding-top: 2px;">Estimated Price</h3>
                                                        <h3 id="price" class="count-desc" style="color: white; font-weight: bold"></h3>
                                                    </div>

                                                </div>
                                            </div> <!-- /.col-sm-3 -->

                                            <div class="input-field col s6" id="locationField">
                                                <input type="checkbox" id="test6" checked="checked"/>
                                                <label for="test6">I accept Truck
                                                    logi<a href="#modal1"> Terms of Service</a> </label>
                                            </div>
                                            <div class="input-field col s6">
                                                <ul class="list-inline pull-right">
                                                    <li>
                                                        <button type="submit"
                                                                class="btn btn-primary next-step z-depth-5 waves-effect waves-light btn">
                                                            <i class="material-icons left">&#xE8AE;</i>Book Now
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="modal1" class="modal">
                                                <div class="modal-content">
                                                    <h4>TruckLogi Terms of services</h4>
                                                    <p>A bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of textA bunch of text</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>

                </div>
            </div>
        </section>
    </div>
</div>