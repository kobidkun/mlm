<link type="text/css" href="https://rawgit.com/guillaumepotier/Parsley.js/2.7.2/src/parsley.css">
<form class="demo-form">
    <div class="form-section">
        <label for="firstname">First Name:</label>
        <input type="text" class="form-control" name="firstname" required="">

        <label for="lastname">Last Name:</label>
        <input type="text" class="form-control" name="lastname" required="">
    </div>

    <div class="form-section">
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email" required="">
    </div>

    <div class="form-section">
        <label for="color">Favorite color:</label>
        <input type="text" class="form-control" name="color" required="">
    </div>

    <div class="form-navigation">
        <button type="button" class="previous btn btn-info pull-left">&lt; Previous</button>
        <button type="button" class="next btn btn-info pull-right">Next &gt;</button>
        <input type="submit" class="btn btn-default pull-right">
        <span class="clearfix"></span>
    </div>

</form>
<style>
    .form-section {
        padding-left: 15px;
        border-left: 2px solid #FF851B;
        display: none;
    }
    .form-section.current {
        display: inherit;
    }
    .btn-info, .btn-default {
        margin-top: 10px;
    }
    html.codepen body {
        margin: 1em;
    }

</style>
