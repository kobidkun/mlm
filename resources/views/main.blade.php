
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600|Lato:700,900,400|Raleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="libs/bower/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="libs/misc/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="libs/bower/animate.css/animate.min.css">
    <link rel="stylesheet" href="assets/css/landing-page.css">
    <title>Phamtom Money - Network Marketing</title>
</head>
@include('components.header')

<header id="header">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="intro-text">
                    <h2 class="section-heading animated">Providing Educational Services</h2>
                    <p class="section-paragraph">Opportunity to Boost Your Career<br> With your Services </p>
                </div>
            </div>
        </div>

        <div id="video-container">

            <img src="img/1.jpg" alt="">

        </div>
    </div><!-- .container -->
</header><!-- #header -->

<div class="modal fade video-modal" id="video-modal" role="dialog">
    <div class="modal-content">
        <iframe src="https://player.vimeo.com/video/160856876" width="860" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
</div><!-- #video-modal -->

<section id="features">
    <div class="container">

        <div class="text-center">
            <h2 class="section-heading animated">What We Do</h2>
            <p class="section-paragraph">
                We beleve that education,more than anything,empowers people to change their lives.
                Phantommoney give education and personality development traning through our video and audio  traning in different different topics.ex-concentration ,how can you concentrate.

            </p>
        </div><!-- .text-center -->

        <div class="row text-center">

            <div class="col-md-4">
                <div class="col-inner feature">
                    <img src="assets/svg/tie.svg" alt="icon">
                    <h4>Study Online</h4>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4">
                <div class="col-inner feature">
                    <img src="assets/svg/pig.svg" alt="icon">
                    <h4>Refer Friends</h4>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4">
                <div class="col-inner feature">
                    <img src="assets/svg/pointer.svg" alt="icon">
                    <h4>Fullfil Your Dreams</h4>
                </div>
            </div><!-- /.col -->

        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- #features -->

<section id="brief">
    <div id="brief-img">
        <img src="assets/images/landing-page/img-1.jpg" alt="">
    </div><!-- #brief-img -->

    <div class="container">
        <div id="brief-text">
            <h2 class="section-heading animated">How to Enroll</h2>
            <p class="section-paragraph">
                Enrol yourself/regester yourself with 200/500 package and get our educational video / audio / training and events according
                to plan


            </p>
            <ul>
                <li>
                    <img class="item-icon" src="assets/svg/check.svg" alt="">
                    <span class="item-text">Register YourSelf</span>
                </li>
                <li>
                    <img class="item-icon" src="assets/svg/check.svg" alt="">
                    <span class="item-text">Pay Joining Fees</span>
                </li>
                <li>
                    <img class="item-icon" src="assets/svg/check.svg" alt="">
                    <span class="item-text">Earn Comisstion Refering Frends & Family</span>
                </li>
            </ul>
        </div><!-- #brief-text -->
    </div><!-- .container -->
</section><!-- #brief -->

<section id="states" class="bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="col-inner text-center">
                    <h3 class="counterUp">100</h3>
                    <h4>Live Videos</h4>
                    <p> Video Material available for you to Study</p>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6">
                <div class="col-inner text-center">
                    <h3 class="counterUp">1000+</h3>
                    <h4>Active Users</h4>
                    <p>1000+ Resgistreed Users Available who are earning  by refering Friends & Family </p>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6">
                <div class="col-inner text-center">
                    <h3 class="counterUp">200</h3>
                    <h4>Audio Coaching</h4>
                    <p> Voices of top instructors on all topics </p>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6">
                <div class="col-inner text-center">
                    <h3 class="counterUp">500</h3>
                    <h4>Digital Copies</h4>
                    <p> Digital Copies available for you to  study</p>
                </div>
            </div><!-- /.col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- #states -->

<section id="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <h2 class="section-heading animated">About Founder</h2>
                    <img src="img/1.jpeg" width="250px">

                    <p class="section-paragraph">
                        <strong>Shams Quamer</strong><br>
                        <strong>Graduated from</strong> :Aligarh muslim universty <br><strong>Experience</strong>: 7 years of successfull experience in network marketing
                    </p>
                </div>
            </div><!-- /.col -->
        </div><!-- .row --><hr>
        
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <h2 class="section-heading animated">Our Team</h2>
                    <img src="img/6.jpg" width="250px">

                    <p class="section-paragraph">
                        <strong>Dilnawaz Ragiv</strong><br>
                        Vp North jone
                    </p>
                </div>
            </div><!-- /.col -->
        </div><!-- .row -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <h2 class="section-heading animated">Contact Us</h2>
                    <p class="section-paragraph">
                        Email: Info@phantommoney.com<br>
                        Phone Number: +91 9572740620 <br>
                    </p>

                </div>
            </div><!-- /.col -->
        </div><!-- .row -->

        <div class="subs-form">
            <form action="#" class="form-horizontal">
                <div class="col-md-4">
                    <div class="control-wrap">
                        <input type="text" class="form-control" placeholder="Your Name">
                        <img src="assets/svg/users.svg" alt="">
                    </div>
                </div><!-- /.col -->

                <div class="col-md-4">
                    <div class="control-wrap">
                        <input type="text" class="form-control" placeholder="Your Mobile">
                        <img src="assets/svg/email.svg" alt="">
                    </div>
                </div><!-- /.col -->

                <div class="col-md-4">
                    <input type="submit" value="Contact Us" class="btn btn-block btn-primary">
                </div><!-- /.col -->
            </form>
        </div>
    </div><!-- .container -->
</section><!-- #subscribe -->




<section id="price">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <h2 class="section-heading animated">Our Prices</h2>
                    <p class="section-paragraph">Choose Between Plan that suits you the Most</p>
                </div>
            </div><!-- /.col -->
        </div><!-- .row -->

        <div class="row">
            <div class="col-md-4 price-column">
                <div class="price-column-inner">
                    <header>
                        <h4> Plan -1</h4>
                        <h3>Rs 200</h3>
                    </header>
                    <ul>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>Limited Video</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>Audio traning </span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>Education and Personality Development</span>
                        </li>
                        
                    </ul>
                    <footer class="text-center">
                        <button class="btn btn-default">BUY NOW</button>
                    </footer>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4 price-column">
                <div class="price-column-inner">
                    <header>
                        <h4> Plan -2</h4>
                        <h3>Rs<span class="counterUp"> 500</span></h3>
                    </header>
                    <ul>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>Un-Limited Video</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>Audio traning</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>Education and Personality Development</span>
                        </li>
                        
                    </ul>
                    <footer class="text-center">
                        <button class="btn btn-primary">BUY NOW</button>
                    </footer>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4 price-column">
                <div class="price-column-inner">
                    <header>
                        <h4>Plan -3</h4>
                        <h3><span class="counterUp">-</span></h3>
                    </header>
                    <ul>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>First sell bonus</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/check.svg" alt="">
                            <span>Level bonus</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>Royalty income</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>First growing traning</span>
                        </li>
                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>Events</span>
                        </li>

                        <li>
                            <img class="item-icon" src="assets/svg/remove.svg" alt="">
                            <span>Much More</span>
                        </li>
                    </ul>
                    <footer class="text-center">
                        <button class="btn btn-default">Contact Us</button>
                    </footer>
                </div>
            </div><!-- /.col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- #price -->



<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="col-inner navigation">
                    <h2>NAVIGATION</h2>
                    <ul class="navigation-menu">
                        <li><a href="#">ABOUT US</a></li>
                        <li><a href="#">SERVICES</a></li>
                        <li><a href="#">SELECTED WORK</a></li>
                        <li><a href="#">GET IN TOUCH</a></li>
                        <li><a href="#">CAREERS</a></li>
                    </ul>
                </div>
            </div><!-- /.col -->

            <div class="col-md-5 col-md-offset-1">
                <div class="col-inner news">
                    <h2>About Us</h2>
                    <p>founded in 2017 Phantom Money its debt free company with office in kolkata and bihar.
                        We are internet focused company we are currently doing busness in several states of india with plan for further in all over india and international market</p>

                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-md-offset-1">
                <div class="col-md-2">
                    <div class="col-inner navigation">
                        <h2>Services</h2>
                        <ul class="navigation-menu">
                            <li><a href="#">Video Tutorial</a></li>
                            <li><a href="#">Audio Tutorial</a></li>
                        </ul>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- #footer -->

<section id="copyright">
    <div class="container text-center">
        <p>Copyright &copy; 2016 Phantom Money </p>
    </div>
</section><!-- #copyright -->
</div>
<div id="loading-div">
    <img src="assets/images/landing-page/puff.svg" width="50" alt="">
</div>
<script src="libs/bower/jquery/dist/jquery.js"></script>
<script src="libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js"></script>
<script src="libs/misc/owl-carousel/owl.carousel.min.js"></script>
<script src="libs/bower/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
<script src="libs/bower/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="libs/bower/counterup/jquery.counterup.min.js"></script>

<script>

    $(function() {
        $(window).on('load', function(){
            $(document.body).addClass('loading-done');
        });

        //= shrink and expand the navbar
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 50) $('.navbar').addClass('shrink');
            else $('.navbar').removeClass('shrink');
        });

        //= autoplay the video when the modal show up
        autoPlayYouTubeModal();

        //= equal columns height
        matchHeight('#states .col-inner');

        //= counterUp plugin
        $('.counterUp').counterUp({ delay: 10, time: 1500 });

        //= set the max-height property for .navbar-collapse
        $(window).on('load resize orientationchange', function(){
            $('.navbar-collapse').css('max-height', $(window).height() - $('.navbar').height());
        });

        $(document).on('click', '[data-toggle="collapse"]', function(e){
            var $trigger = $(e.target);
            $trigger.is('[data-toggle="collapse"]') || ($trigger = $trigger.parents('[data-toggle="collapse"]'));
            var $target = $($trigger.attr('data-target'));
            if ($target.attr('id') === 'page-menu-collapse')
                $(document.body).toggleClass('navbar-collapse-show', !$trigger.hasClass('collapsed'))
        });

        //= initialize smooth scroll plugin
        smoothScroll.init({
            offset: 60,
            speed: 1000,
            updateURL: false
        });

        // initializing owlCarousel
        $("#owl-slider").owlCarousel({
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay: true
        });

        // initialize waypoints for section-headings
        $('.section-heading').waypoint(function(direction) {
            if (direction === 'down') $(this.element).addClass('fadeInUp');
            else $(this.element).removeClass('fadeInUp');
        }, { offset: '96%' });
    });


    matchHeight = function(selector){
        var height, max = 0, $selector = $(selector);
        $selector.each(function(index, item){
            height = $(item).height();
            if (height > max) max = height;
        });
        $selector.height(max);
    };
</script>
</body>

</html>